import React from "react";
import * as R from "ramda";
import Card from "../Card";

const Faqs = ({ questions }) => (
  <Card title="FAQ">
    <div className="questions">
      {R.map(
        q => (
          <div key={q} className="faq-question">
            {q}
          </div>
        ),
        questions
      )}
    </div>
    <div className="row list-ext-btn btn">
      <span>See all questions</span>
    </div>
  </Card>
);

export default Faqs;
