import React from "react";
import Expenditure from "./Expenditure";
import Profile from "./Profile";
import Faqs from "./Faqs";
import Merchants from "./Merchants";
import Transactions from "./Transactions";
import { transactions, chartData, questions, merchants } from "./constants";

const Overview = props => (
  <section className="section tab-container">
    <div className="container overview-container is-flex">
      <div className="dash-col">
        <Expenditure totalSpent="200" spendLimit="700" meter="25%" />
        <Transactions transactions={transactions} />
      </div>
      <div className="dash-col">
        <Profile
          userName="John Doe"
          creditScore={3.5}
          device="Oneplus 3T + 2 more"
          phoneNumber="9870763023"
          chartData={chartData}
          totalTransactions={22}
        />
        <Faqs questions={questions} />
        <Merchants merchants={merchants} />
      </div>
    </div>
  </section>
);
Overview.propTypes = {};

export default Overview;
