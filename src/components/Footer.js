import React from "react";
import * as R from "ramda";
import MediaQuery from "react-responsive";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel
} from "react-accessible-accordion";

const simplLinks = ["About", "Testimonials", "Careers", "Help"];
const socialLinks = ["Facebook", "Twitter", "Linkedin", "Medium"];
const legalLinks = ["Terms", "Privacy Policy"];
const appLinks = ["Android App", "iOS App"];

const Footer = () => (
  <footer className="section">
    <div className="container">
      <MediaQuery query="(max-width: 768px)">
        <FooterMobile />
      </MediaQuery>
      <MediaQuery query="(min-width: 769px)">
        <FooterDesktop />
      </MediaQuery>
    </div>
  </footer>
);

const FooterDesc = () => (
  <div className="simpl-footer-desc">
    <div className="footer-logo" />
    <div className="footer-desc">
      Our mission is to make money simple, so that people can live well and do
      amazing things.
    </div>
    <div className="copyright">© Copyright 2018</div>
  </div>
);

const FooterDesktop = () => (
  <React.Fragment>
    <div className="row footer-links-row">
      <FooterDesc />
      <div className="footer-links simpl-links">
        <span className="lnk-title">SIMPL</span>
        {footerLinks(simplLinks)}
      </div>
      <div className="footer-links social-media-links">
        <span className="lnk-title">SOCIAL MEDIA</span>
        {footerLinks(socialLinks)}
      </div>
      <div className="footer-links legal-links">
        <span className="lnk-title">LEGAL</span>
        {footerLinks(legalLinks)}
      </div>
      <div className="footer-links get-app-links">
        <span className="lnk-title">GET APP</span>
        {footerLinks(appLinks)}
      </div>
    </div>
    <div className="row footer-addr">
      <div className="address">
        Get Simpl Technologies Private Ltd.
        <br />
        Address: No. 811, Crescent Business Park, Sakinaka Telephone Exchange
        Lane, Andheri East, Mumbai - 400072
        <br />
        Email: help [at] getsimpl.com, CIN no: U74999MH2015PTC261546
      </div>
      <div className="google-apple-tm">
        *Google Play and the Google Play logo are trademarks of Google Inc.
        *Apple® and the Apple logo are trademarks of Apple Inc., registered in
        the U.S. and other countries. App Store® is a service mark of Apple Inc.
      </div>
    </div>
  </React.Fragment>
);

const FooterMobile = () => (
  <React.Fragment>
    <Accordion>
      <AccordionItem>
        <AccordionItemHeading>
          <AccordionItemButton>SIMPL</AccordionItemButton>
        </AccordionItemHeading>
        <AccordionItemPanel>{footerLinks(simplLinks)}</AccordionItemPanel>
      </AccordionItem>
      <AccordionItem>
        <AccordionItemHeading>
          <AccordionItemButton>SOCIAL MEDIA</AccordionItemButton>
        </AccordionItemHeading>
        <AccordionItemPanel>{footerLinks(socialLinks)}</AccordionItemPanel>
      </AccordionItem>
      <AccordionItem>
        <AccordionItemHeading>
          <AccordionItemButton>LEGAL</AccordionItemButton>
        </AccordionItemHeading>
        <AccordionItemPanel>{footerLinks(legalLinks)}</AccordionItemPanel>
      </AccordionItem>
      <AccordionItem>
        <AccordionItemHeading>
          <AccordionItemButton>GET APP</AccordionItemButton>
        </AccordionItemHeading>
        <AccordionItemPanel>{footerLinks(appLinks)}</AccordionItemPanel>
      </AccordionItem>
    </Accordion>
    <FooterDesc />
  </React.Fragment>
);

const footerLinks = R.map(l => (
  <a key={l} href="#" className="footer-lnk">
    {l}
  </a>
));

export default Footer;
