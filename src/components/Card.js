import React from "react";

const Card = ({ title, children }) => (
  <div className="dash-card">
    <div className="dash-title">{title}</div>
    <div className="dash-card-box">{children}</div>
  </div>
);

export default Card;
