import React from "react";
import * as R from "ramda";
import Card from "../Card";

const Merchants = ({ merchants }) => (
  <Card title={"MERCHANTS WE SUPPORT"}>
    <div className="merchant-list is-flex">
      {R.map(
        ({ name, osList }) => (
          <div key={name} className="merchant">
            <div className={`merchant-logo merchant-${name}`} />
            <span>{osList}</span>
          </div>
        ),
        merchants
      )}
    </div>
    <div className="row list-ext-btn btn">
      <span>See all +44 merchants</span>
    </div>
  </Card>
);

export default Merchants;
