export const transactions = [
  { label: "Faasos", value: "180", date: "May 27, 04:25 PM", type: "bill" },
  { label: "Bookmyshow", value: "180", date: "May 27, 04:25 PM", type: "bill" },
  { label: "DocsApp", value: "180", date: "May 27, 04:25 PM", type: "bill" },
  {
    label: "You Paid Simpl",
    value: "560",
    date: "May 27, 04:25 PM",
    type: "bill-paid"
  },
  { label: "HolaChef", value: "180", date: "May 27, 04:25 PM", type: "bill" },
  {
    billValue: "560",
    dateStart: "May 27, 04:25 PM",
    dateEnd: "May 27, 04:25 PM",
    dueDate: "May 27, 04:25 PM",
    type: "bill-generated"
  },
  { label: "Refund", value: "180", date: "May 27, 04:25 PM", type: "bill" },

  { label: "FreshMenu", value: "180", date: "May 27, 04:25 PM", type: "bill" }
];

export const chartData = [
  {
    title: "Food",
    value: 10,
    color: "#00d1c1"
  },
  {
    title: "Personal Care",
    value: 15,
    color: "#ff9600"
  },
  {
    title: "Clothes",
    value: 20,
    color: "#ff5950"
  },
  {
    title: "Travel",
    value: 20,
    color: "#77898a"
  }
];

export const questions = [
  "How does Simpl work?",
  "How is my Spending limit determined?",
  "How does Simpl’s billing cycle works?"
];

export const merchants = [
  { name: "bookmyshow", osList: "Android" },
  { name: "freshmenu", osList: "Android iOS Web" },
  { name: "faasos", osList: "Android iOS Web" },
  { name: "zomato", osList: "iOS" }
];
