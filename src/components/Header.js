import React, { useState } from "react";
import PropTypes from "prop-types";
import Tabs from "./Tabs";
import MediaQuery from "react-responsive";

const tablist = [
  { title: "OVERVIEW" },
  { title: "PROFILE" },
  { title: "TRANSACTIONS" },
  { title: "MERCHANTS" },
  { title: "HELP" }
];

const Header = ({ name, lastTransacted, nextBillDate }) => {
  const [selectedTab, setSelectedTab] = useState("OVERVIEW");
  return (
    <header className="section simpl-header">
      <div className="container">
        <div className="header-logo" />
        <div className="header-ham-menu" />
        <div className="header-download-logout is-flex">
          <span>DOWNLOAD APP</span>
          <span>LOGOUT</span>
        </div>
        <MediaQuery query="(max-width: 768px)">
          <div className="header-welcome">
            Welcome back{" "}
            <span role="img" aria-label="Welcome">
              👋
            </span>
          </div>
          <div className="header-welcome">{name}</div>
        </MediaQuery>
        <MediaQuery query="(min-width: 769px)">
          <div className="header-welcome">{`Welcome back, ${name}`}</div>
        </MediaQuery>

        <div className="header-summary">
          You last transacted on <span>{lastTransacted}</span>. Your next bill
          will be generated on <span>{nextBillDate}</span>.
        </div>
        <Tabs
          tabs={tablist}
          selected={selectedTab}
          onTabSelect={setSelectedTab}
        />
      </div>
    </header>
  );
};
Header.propTypes = {
  name: PropTypes.string,
  lastTransacted: PropTypes.string,
  nextBillDate: PropTypes.string
};

export default Header;
