import React from "react";
import Card from "../Card";

const Expenditure = ({ totalSpent, spendLimit, meter }) => (
  <Card title="EXPRENDITURE">
    <div className="expense-details is-flex">
      <div className="col lft">
        <div className="lbl">Total Expense</div>
        <div className="val">{totalSpent}</div>
      </div>
      <div className="col rgt">
        <div className="lbl">Spend Limit</div>
        <div className="val">{spendLimit}</div>
      </div>
    </div>
    <div className="expense-progress">
      <span style={{ width: meter }} />
    </div>
    <div className="pay-now-btn btn">
      <span>PAY NOW</span>
    </div>
  </Card>
);

export default Expenditure;
