import React from "react";
import Card from "../Card";
import PieChart from "react-minimal-pie-chart";

const Profile = ({
  userName,
  creditScore,
  device,
  phoneNumber,
  chartData,
  totalTransactions
}) => (
  <Card title="PROFILE">
    <div className="user-profile-details">
      <div className="user-dp" />
      <div className="user-name">{userName}</div>
      <div className="user-credit-score">
        <span>Simpl Credit Score</span>
        <span className="credit-score">
          <b>{creditScore}</b> /10
        </span>
      </div>
    </div>
    <div className="user-phone-details">
      <div className="user-device">{device}</div>
      <div className="user-phone">{phoneNumber}</div>
    </div>
    <div className="user-total-transactions">
      <div className="title">Total Transactions</div>
      <div className="transaction-details is-flex">
        <div className="col pie-chart">
          <PieChart
            data={chartData}
            lineWidth={30}
            paddingAngle={5}
            lengthAngle={-360}
            style={{ height: "100px", width: "100px" }}
          />
          <span className="chart-total">{totalTransactions}</span>
        </div>
        <div className="col chart-index">
          <div className="chart-index-food">Food</div>
          <div className="chart-index-personal">Personal Care</div>
          <div className="chart-index-clothes">Clothes</div>
          <div className="chart-index-travel">Travel</div>
        </div>
      </div>
    </div>
    <div className="row list-ext-btn btn">
      <span>Your Profile</span>
      <span className="profile-status">Incomplete</span>
    </div>
  </Card>
);

export default Profile;
