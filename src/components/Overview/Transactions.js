import React from "react";
import Card from "../Card";
import * as R from "ramda";

const Transactions = ({ transactions }) => (
  <Card title="TRANSACTIONS">
    <div className="transaction-list">
      {R.map(item => {
        if (item.type === "bill") {
          return (
            <BillItem
              key={`${item.label}-${item.val}`}
              label={item.label}
              value={item.value}
              date={item.date}
            />
          );
        }
        if (item.type === "bill-paid") {
          return (
            <BillPaidItem
              key={`${item.label}-${item.val}`}
              label={item.label}
              value={item.value}
              date={item.date}
            />
          );
        }
        if (item.type === "bill-generated") {
          return (
            <BillGeneratedItem
              key={`gen-${item.billValue}`}
              billValue={item.billValue}
              dateStart={item.dateStart}
              dateEnd={item.dateEnd}
              dueDate={item.dueDate}
            />
          );
        }
        return null;
      }, transactions)}
    </div>
    <div className="row list-ext-btn btn">
      <span>See all transactions</span>
    </div>
  </Card>
);

const BillItem = ({ label, value, date }) => (
  <div className="transaction-item transaction-bill-item is-flex">
    <div className="col lft">
      <div className="lbl">{label}</div>
      <div className="date">{date}</div>
    </div>
    <div className="col rgt">
      <div className="val">{value}</div>
      <div className="lbl">More</div>
    </div>
  </div>
);
const BillPaidItem = ({ label, value, date }) => (
  <div className="transaction-item transaction-bill-paid-item is-flex">
    <div className="col lft">
      <div className="lbl">{label}</div>
      <div className="date">{date}</div>
    </div>
    <div className="col rgt">
      <div className="val">
        <span>{value}</span>
      </div>
      <div className="lbl">Thank you</div>
    </div>
  </div>
);
const BillGeneratedItem = ({ billValue, dateStart, dateEnd, dueDate }) => (
  <div className="transaction-item transaction-bill-generated-item">
    <div className="row bill">
      <div className="bill-gen-details">
        <div className="title">{`Bill Generated | ${billValue}`}</div>
        <div className="bill-summary">{`For transactions made between ${dateStart} to ${dateEnd}`}</div>
      </div>
    </div>
    <div className="row pay">
      <div className="pay-now-btn btn small">PAY NOW</div>
      <div className="bill-due">{`Due by ${dueDate}`}</div>
    </div>
  </div>
);

export default Transactions;
