import React from "react";
import Header from "./components/Header";
import Overview from "./components/Overview";
import Footer from "./components/Footer";

const App = () => (
  <React.Fragment>
    <Header
      name="John Doe"
      lastTransacted="Faasos"
      nextBillDate="12 June 2017"
    />
    <Overview />
    <Footer />
  </React.Fragment>
);

export default App;
