import React from "react";
import PropTypes from "prop-types";
import * as R from "ramda";
import "../styles/tabs.scss";

const Tabs = ({ tabs, selected, onTabSelect }) => (
  <div className="simpl-nav-tabs is-flex">
    {R.map(
      tab => (
        <div
          key={tab.title}
          className={`simpl-tab btn ${
            selected === tab.title ? "selected" : ""
          }`}
          onClick={() => onTabSelect(tab.title)}
        >
          {tab.title}
        </div>
      ),
      tabs
    )}
    <span className={`simpl-tab-selected-border ${R.toLower(selected)}`} />
  </div>
);
Tabs.propTypes = {
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      link: PropTypes.string
    })
  ).isRequired,
  selected: PropTypes.string.isRequired
};

export default Tabs;
